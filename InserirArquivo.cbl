       program-id. InserirArquivo as "InserirArquivo".

       environment division.
       configuration section.
           	SPECIAL-NAMES.
           	DECIMAL-POINT   IS   COMMA.

       data division.
       working-storage section.
           77  BARRA-DUPLA		PIC	X(74)	  VALUE ALL  "=".
           77  NUM_COMPRA       PIC 9(5)      VALUES ZEROS.
           77  DESCRICAO        PIC X(30)     VALUES SPACES.
           77  NUM_CARTAO       PIC 9(16)     VALUES ZEROS.
           77  VALOR            PIC 9(4)V99   VALUES ZEROS.
           77  VALOR-ED         PIC	Z.ZZ9,99  VALUE ZEROS.
           77  OPC              PIC X(1)      VALUES " ".
       
       SCREEN SECTION.
       01 TELA-COR.
           02 BLANK SCREEN BACKGROUND-COLOR 4.

       procedure division.
           INICIO.
           DISPLAY TELA-COR.
           
           PERFORM PROCESSAMENTO UNTIL OPC = "N" or "n".
               DISPLAY "FIM PROCESSAMENTO" AT 2460.
               STOP " ".
               STOP RUN.
           PROCESSAMENTO.
           
           
           MOVE SPACES TO DESCRICAO OPC.
           MOVE ZEROS TO NUM_COMPRA NUM_CARTAO VALOR VALOR-ED.

           DISPLAY BARRA-DUPLA AT 0505.
           DISPLAY "NUM_COMPRA" at 1010.
           DISPLAY "DESCRICAO:" at 1210.
           DISPLAY "NUM_CARTAO:" at 1410.
           DISPLAY "VALOR:" at 1610.
           
           ACCEPT NUM_COMPRA at 1030
           ACCEPT DESCRICAO at 1230
           ACCEPT NUM_CARTAO at 1430
           ACCEPT VALOR-ED at 1630
               MOVE VALOR-ED TO VALOR
               
           DISPLAY "OUTRO REGISTRO (S/N)" AT 2010.
           PERFORM WITH TEST AFTER UNTIL OPC = "S" or "N"
               ACCEPT OPC AT 2035 WITH UPPER AUTO
               IF OPC NOT = "S" AND "N"
                   DISPLAY "DIGITE S OU N" AT 2040
               ELSE
                   DISPLAY "             " AT 2040
               END-IF
           END-PERFORM.

       end program InserirArquivo.
